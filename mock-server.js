const express = require('express')
const app = express()
const port = 8000

const validJson = {
    data: "Here are som data!!!! numbero dos"
};
const notValidJson = "This is not valid json";

app.get('/valid', (req, res) => {
    res.set('Access-Control-Allow-Origin', 'http://localhost:3001');
    res.send(validJson)
})
app.get('/invalid', (req, res) => {
    res.set('Access-Control-Allow-Origin', 'http://localhost:3001');
    res.send(notValidJson)
})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})
