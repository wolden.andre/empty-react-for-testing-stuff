import React, {useEffect, useState} from 'react';
import {Either, fold, left, right} from "fp-ts/Either";
import {pipe} from "fp-ts/pipeable";

interface ApiData {
  datab: string;
}

const isApiData = (input: any): input is ApiData =>
  input &&
  input.datab;

enum CustomError {
  OTHER = "OTHER",
  INVALID_JSON = "INVALID_JSON",
  INVALID_TYPE = "INVALID_TYPE",
}

function promiseType<E, T>(typeError: E, typeguard: (input: any) => input is T): (input: any) => Promise<T>{
  return function<E, T> (input: any) {
    if (typeguard(input)) {
      return Promise.resolve(input);
    } else {
      throw typeError;
    }
  }
}

/*
Hva jeg forventer:

Henter valid data.
Henter invalid data.
thrower exception på res.json()
blir catch'a av catch block
 */
const catchInCatchNecessaryQuestion = (): Promise<Either<CustomError, ApiData>> =>
  fetch("http://localhost:8000/valid")
    .then(res =>
      fetch("http://localhost:8000/invalid")
        .then(res => res.json()))
    .then(promiseType<CustomError, ApiData>(CustomError.INVALID_TYPE, isApiData))
    .then(apiData => right(apiData))
    .catch(e => {
      switch (e) {
        case CustomError.INVALID_TYPE:
          return left(e)
        default:
          return left(CustomError.OTHER)
      }
    });


const fetchAndSucceed = (): Promise<any> => fetch("http://localhost:8000/valid")
  .then(response => {
    console.info("fetchAndSucceed -> then")
    throw new Error("I refuuuuuse!")
    // return response.json();
  })
  .catch((e: Error) => {
    console.info("fetchAndSucceed -> catch")
    throw e;
  });

const fetchAndFail = (): Promise<ApiData> => fetch("http://localhost:8000/invalid")
  .then(response => response.json())
  .catch((e: Error) => {
    return e;
  });

const PromisePrac = () => {

  const [data, setData] = useState<Either<CustomError, ApiData> | undefined>(undefined);

  useEffect(() => {
    if (data === undefined) {
      // fetchAndSucceed()
      //   .then((maybeApiData: ApiData) => {
      //     console.warn("THEN")
      //   })
      //   .catch(e => {
      //     console.warn("CATCH")
      //   })

      catchInCatchNecessaryQuestion()
        .then((res: Either<CustomError, ApiData>) => setData(res))
    }
  });

  const viewString = (data: Either<CustomError, ApiData>) => pipe(
    data,
    fold<CustomError, ApiData, string>(
      error => error.toString(),
      apiData => "Real API data: " + JSON.stringify(apiData)
    )
  );

  if (data) {
    return (
      <div>{viewString(data)}</div>
    );
  } else {
    return (
      <div>Data is undefined (i.e. initial state)</div>
    );
  }
};

export default PromisePrac;
