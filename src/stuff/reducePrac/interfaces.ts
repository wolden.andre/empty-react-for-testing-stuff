export interface Agreement {
  id: number;
  title: string;
}

export const isAgreement = (input: any): input is Agreement =>
  input &&
  input.id &&
  input.title !== undefined;

export const isAgreements = (input: any): input is Agreement[] =>
  input &&
  Array.isArray(input) &&
  input.reduce((prev, curr) => prev && isAgreement(curr), true);
