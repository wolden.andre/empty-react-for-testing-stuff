import {Agreement, isAgreements} from "./interfaces";

const ReducePracView = () => {

  const validAgreementOne: Agreement = {
    id: 1,
    title: "Valid agreement one"
  };

  const invalidAgreementOne: any = {
    id: 3,
  };


  const validArrayOne: Agreement[] = [];
  const validArrayTwo = [
    validAgreementOne,
    validAgreementOne,
    validAgreementOne
  ];

  const invalidArrayOne: Agreement[] = [
    validAgreementOne,
    validAgreementOne,
    invalidAgreementOne,
    validAgreementOne,
    validAgreementOne
  ];

  const oneIsValid: boolean = isAgreements(validArrayOne);
  const twoIsValid: boolean = isAgreements(validArrayTwo);
  const threeIsInvalid: boolean = isAgreements(invalidArrayOne);

  return (
    <div>
      <div>This is the reduce test.</div>
      <div>#1, should be true, result = {JSON.stringify(oneIsValid)}</div>
      <div>#2, should be true, result = {JSON.stringify(twoIsValid)}</div>
      <div>#3, should be false, result = {JSON.stringify(threeIsInvalid)}</div>
    </div>
  )

};

export default ReducePracView;
